package com.example.tpnote;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import data.collection;
import data.collectionRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private collectionRepository repository;
    private MutableLiveData<collection> city;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;

    public DetailViewModel (Application application) {
        super(application);
        repository = collectionRepository.get(application);
        collection = new MutableLiveData<>();
        isLoading = new MutableLiveData<>();
        webServiceThrowable = new MutableLiveData<>();
    }

    public void setIsLoading() {
        isLoading = repository.getIsLoading();
    }

    public MutableLiveData<Boolean> getIsLoading(){
        return isLoading;
    }


    public void setWebServiceThrowable(){
        webServiceThrowable = repository.getWebServiceThrowable();
    }

    public MutableLiveData<Throwable> getWebServiceThrowable(){
        return webServiceThrowable;
    }


    public void setCity(long id) {
        repository.getcollection(id);
        city = repository.getSelectedcollection();
    }

    public void loadWeatherCity(){
        repository.loadcollection(city.getValue());
    }
    LiveData<collection> getCity() {
        return collection;
    }
}



package com.example.tpnote;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import data.collection;
import data.collectionRepository;

public class ListViewModel extends AndroidViewModel {
    private collectionRepository repository;
    private LiveData<List<collection>> allcollection;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;

    public ListViewModel (Application application) {
        super(application);
        repository = collectionRepository.get(application);
        allcollection = repository.getAllcollection();
        isLoading = new MutableLiveData<>();
        webServiceThrowable = new MutableLiveData<>();
    }

    public void setIsLoading() {
        isLoading = repository.getIsLoading();
    }

    public MutableLiveData<Boolean> getIsLoading(){
        return isLoading;
    }


    public void setWebServiceThrowable(){
        webServiceThrowable = repository.getWebServiceThrowable();
    }

    public MutableLiveData<Throwable> getWebServiceThrowable(){
        return webServiceThrowable;
    }

    LiveData<List<collection>> getAllCities() {
        return allcollection;
    }



}

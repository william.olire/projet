package com.example.tpnote;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailFragment viewModel;
    private TextView textname, textcategories, textdescription, textbrand, texttechnicalDetails;
    private ImageView imgpictures, imgthumbnail;
    private TextView texttimeFrame , textyear;


    private ImageView imgWeather;
    private ProgressBar progress;
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    private void listenerSetup() {
        textname = getView().findViewById(R.id.textname);
        textcategories = getView().findViewById(R.id.textcategories );
        textdescription = getView().findViewById(R.id.textdescription);
        texttimeFrame = getView().findViewById(R.id.texttimeFrame);
        textyear= getView().findViewById(R.id.textyears);
        textbrand= getView().findViewById(R.id.textbrand);
        texttechnicalDetails = getView().findViewById(R.id.texttechnicalDetails);
        imgpictures = getView().findViewById(R.id.imgpictures);
        imgthumbnail = getView().findViewById(R.id.imgthumbnail);



    }


}
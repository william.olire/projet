package data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;
@Dao
public interface collectionDAO {
    @Query("SELECT * from collection ORDER BY name ASC")
    LiveData<List<collection>> getAllCollection();

    @Query("SELECT * FROM collection WHERE itemID = :id")
    collection getcollectionById(String id);

}

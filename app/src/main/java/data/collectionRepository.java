package data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class collectionRepository {
    private static final String TAG = collectionRepository.class.getSimpleName();

    private LiveData<List<collection>> allcollection;
    private MutableLiveData<collection> selectedcollection;
    private final OWMInterface api;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;
    private volatile Integer nbAPIloads;

    private collectionDAO collectionDao;


    public MutableLiveData<Boolean> getIsLoading(){
        return isLoading;
    }

    public MutableLiveData<Throwable> getWebServiceThrowable(){
        return webServiceThrowable;
    }

    private static volatile collectionRepository INSTANCE;

    public synchronized static collectionRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new collectionRepository(application);
        }

        return INSTANCE;
    }
    public collectionRepository(Application application) {
        collectionRoomDatabase db = collectionRoomDatabase.getDatabase(application);
        collectionDao = db.collectionDAO();
        allcollection = collectionDao.getAllcollection();
        selectedcollection = new MutableLiveData<>();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(OWMInterface.class);
        isLoading = new MutableLiveData<>();
        webServiceThrowable = new MutableLiveData<>();
    }

    public LiveData<List<collection>> getAllcollection() {
        return allcollection;
    }

    public MutableLiveData<collection> getSelectedcollection() {
        return selectedcollection;
    }


    public void loadWeatherAllcollection(){
        List<collection> collections = null;

        Future<List<collection>> querycollection = databaseWriteExecutor.submit(() -> {
            return collectionDAO.getSynchrAllcollection();
        });
        try {
            collections = querycollection.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        nbAPIloads = collections.size();
        for (int i = 0; i < collections.size(); i++) {
            loadcollectionCity(collections.get(i));
        }
    }

    public void getcollection(String id)  {
        Future<collection> fcollection = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return collectionDAO.getcollectionById(id);
        });
        try {
            selectedcollection.setValue(fcollection.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void loadcollectionCity(collection collection){

        isLoading.postValue(true);
        webServiceThrowable.postValue(null);

        api.getForecast(collection.getName()+",,"+ collection.getcollectionCode(), "bfcd149ff33d38724f4526df85bf43fc").enqueue(
                new Callback<collectionResponse>() {
                    @Override
                    public void onResponse(Call<collectionResponse> call,
                                           Response<collectionResponse> response) {
                        collectionResult.transferInfo(response.body(), collection);
                        if(nbAPIloads != null){
                            nbAPIloads--;
                            if(nbAPIloads == 0){
                                isLoading.postValue(false);
                                nbAPIloads = null;
                            }
                        }else{
                            isLoading.postValue(false);
                        }
                    }
                    @Override
                    public void onFailure(Call<collectionResponse> call, Throwable t) {
                        webServiceThrowable.postValue(t);
                        if(nbAPIloads != null){
                            nbAPIloads--;
                            if(nbAPIloads == 0){
                                isLoading.postValue(false);
                                nbAPIloads = null;
                            }
                        }else{
                            isLoading.postValue(false);
                        }
                    }
                });

    }


}











package data;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface OWMInterface {
    @Headers("Accept: application/geo+json")
    @GET("collection")
    Call<collection> getForecast(@Query("q") String query,
                                      @Query("APIkey") String apiKey);
}


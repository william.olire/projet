package data;

public class collectioResult {
    public static void transferInfo(collectionResponse collectionInfo, collection cityInfo){
        collectionInfo.setitemID(collectionInfo.sys.country);
        cityInfo.setDescription(collectionInfo.collection.get(0).description);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setTemperature(weatherInfo.main.temp);
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setWindDirection(weatherInfo.wind.deg);
        cityInfo.setTempKelvin(weatherInfo.main.temp);
        cityInfo.setWindSpeedMPerS(weatherInfo.wind.speed);
        cityInfo.setLastUpdate(weatherInfo.dt);
    }
}
